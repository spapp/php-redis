
default: test

doc:
	/usr/bin/phpdoc -c phpdoc.ini

test:
	cd test;phpunit

.PHONY: default doc test