<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2014
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   php_redis
 * @since     2014.03.31.
 */
ini_set('display_errors', true);
ini_set('display_startup_errors', true);
error_reporting(E_ALL | E_STRICT | E_DEPRECATED);

define('APPLICATION_PATH', dirname(__DIR__));

if ('cli' !== substr(php_sapi_name(), 0, 3)) {
    throw new Exception('Use this script in CLI.');
}

require_once(dirname(APPLICATION_PATH) . '/php-autoloader/library/Autoloader.php');
Autoloader::getInstance()->addIncludePath(APPLICATION_PATH . '/library')->register();

use Redis\Client as RedisClient;
use Redis\Command as RedisCommand;
use Redis\Command\Exception as RedisCommandException;
use Redis\Connection as RedisConnection;
use Redis\Connection\Tcp as RedisConnectionTcp;

if ($argc < 1) {
    echoHelp();
    exit(0);
}

$command = '';
$bold    = "\033[1;37m";
$normal  = "\033[0;37m";
$params  = array();
$db      = false;
$options = array(
        RedisConnection::OPTION_TIMEOUT            => RedisConnection::DEFAULT_TIMEOUT,
        RedisConnection::OPTION_CONNECTION_TIMEOUT => RedisConnection::DEFAULT_CONNECTION_TIMEOUT,
        RedisConnection::OPTION_PROTOCOL           => RedisConnection::PROTOCOL_TCP,
        RedisConnection::OPTION_HOST               => RedisConnectionTcp::DEFAULT_HOST,
        RedisConnection::OPTION_PORT               => RedisConnectionTcp::DEFAULT_PORT,
        RedisConnection::OPTION_PATH               => '',
        RedisConnection::OPTION_ASYNC              => false,
        RedisConnection::OPTION_PERSIST            => false
);

// remove script name
array_shift($argv);

while (count($argv) > 0) {
    $arg = array_shift($argv);

    switch ($arg) {
        case '--help':
        case '-h':
            echoHelp();
            exit(0);
            break;
        case '-db':
            $db = array_shift($argv);
            break;
        case '--command':
        case '-c':
            $command = array_shift($argv);
            break;
//        case '--'.RedisConnection::OPTION_TIMEOUT:
//            $options[substr($arg, 2)] = array_shift($argv);
//            break;

        default:
            array_push($params, $arg);
    }
}

//if (false === RedisCommand::isExists($command)) {
//    throw new RedisCommandException('Not supported command (' . strtoupper($command) . ').');
//}

$redisClient = RedisClient::create($options);
$redisClient->setResponseFetchMode(RedisClient::FETCH_MODE_OBJECT);

if (false !== $db) {
    $redisClient->select($db);
}


//$r = $redisClient->info('server');
////$r = $redisClient->client('kill', '127.0.0.1',37906);
//
////$r = $redisClient->client('getname');
////$r = $redisClient->client('pause', 10000);
//var_export($r->getValue());
//
//echo PHP_EOL.PHP_EOL.$r->getRawValue();
//
//exit;

execCommand($redisClient, $command, $params, $options, $db);

function execCommand($redisClient, $command, $params = array(), $options, $db = 0) {
    $bold   = "\033[1;37m";
    $normal = "\033[0;37m";

    $response = call_user_func_array(array(
                                             $redisClient,
                                             $command
                                     ),
                                     $params);

    $responseValue = $response->getValue();


    echo PHP_EOL . $bold . 'SERVER:' . $normal . $options[RedisConnection::OPTION_HOST] . ' ( ' . ((int)$db) . ' )' .
         PHP_EOL;
    echo PHP_EOL . $bold . 'COMMAND' . $normal . PHP_EOL;
    echo $command . '( ', implode(', ', $params) . ' )';
    echo PHP_EOL . PHP_EOL;
    echo $bold . 'RESPONSE' . $normal . PHP_EOL;

    if (is_array($responseValue)) {
        print_r($responseValue);
    } else {
        var_export($responseValue);
    }

    echo PHP_EOL . PHP_EOL;
    echo $bold . 'RAW RESPONSE' . $normal . PHP_EOL;
    echo $response->getRawValue();

    echo PHP_EOL . PHP_EOL;
}

/**
 * Display help
 *
 * @return void
 */
function echoHelp() {
    $bold   = "\033[1;37m";
    $normal = "\033[0;37m";

    echo <<<EOF
$bold
NAME $normal
       redis.php - exec a redis command
$bold
SYNOPSIS
       php redis.php $normal [OPTION]... [PARAMS]...
$bold
DESCRIPTION
    -c, --command $normal
        redis command name
    $bold
    --connectionTimeout $normal
        redis connection timeout  (default: 60)
    $bold
    --db $normal
        redis database index
    $bold
    -h, --help $normal
        display this help and exit
    $bold
    --host $normal
        database host (default: '127.0.0.1')
    $bold
    --path $normal
        (default: '')
    $bold
    --persist $normal
        (default: false)
    $bold
    --port $normal
        database port (default: 6379)
    $bold
    --protocol $normal
        conection protocol (default: 'tcp')
    $bold
    --async $normal
        (default: false)

EOF;

}

