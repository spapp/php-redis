<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2014
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   php_redis
 * @since     2014.03.31.
 */
namespace Redis;

use Redis\Command;
use Redis\Command\AbstractCommand;
use Redis\Command\Exception as CommandException;
use Redis\Command\Filter as CommandFilter;
use Redis\Command\Queue as CommandQueue;
use Redis\Connection;
use Redis\Connection\ConnectionInterface;
use Redis\Response;
use Redis\Response\Filter as ResponseFilter;

/**
 * Class Redis\Client
 *
 * @link http://redis.io/commands#connection Connection
 * @method bool auth(string $password) Authenticate to the server
 * @method string echo (string $message) Returns message
 * @method bool ping() Ping the server
 * @method bool quit() Close the connection
 * @method bool select(int $index) Change the selected database for the current connection
 *
 * @link http://redis.io/commands#generic Keys
 * @method bool del(string $key0, string $keyN = '') Delete a key
 * @method string dump(string $key) Return a serialized version of the value stored at the specified key
 * @method bool exists(string $key) Determine if a key exists
 * @method bool expire(string $key, int $secunds) Set a key's time to live in seconds
 * @method bool expireat(string $key, int $timestamp) Set the expiration for a key as a UNIX timestamp
 * @method array keys(string $pattern) Find all keys matching the given pattern
 * @method bool migrate(string $host, int $port, string $key, int $destinationDb, int $timeout, string $copyOrReplace) Atomically transfer a key from a Redis instance to another one
 * @method bool move(string $key, int $db) Move a key to another database
 * @method mixed object(string $subCommand, string $arg0, string $argN) Inspect the internals of Redis objects
 * @method bool persist(string $key) Remove the expiration from a key
 * @method bool pexpire(string $key, int $millisecunds) Set a key's time to live in milliseconds
 * @method bool pexpireat(string $key, int $millisecondsTimestamp) Set the expiration for a key as a UNIX timestamp specified in milliseconds
 * @method int pttl(string $key) Get the time to live for a key in milliseconds
 * @method string randomkey() Return a random key from the keyspace
 * @method bool rename(string $key, string $newKey) Rename a key
 * @method bool renamenx(string $key, string $newKey) Rename a key, only if the new key does not exist
 * @method bool restore(string $key, int $ttl, string $serializedValue) Create a key using the provided serialized value, previously obtained using DUMP
 * @method array sort(string $key, array $options) Sort the elements in a list, set or sorted set
 * @method int ttl(string $key) Get the time to live for a key
 * @method string type(string $key) Determine the type stored at key
 * @method array scan(int $cursor, string $pattern, int $count) Incrementally iterate the keys space
 *
 * @link http://redis.io/commands#string Strings
 * @method int append(string $key, string $value) Append a value to a key. Returns the length of the string after the append operation.
 * @method int bitcount(string $key, int $start, int $end) Count set bits in a string
 * @method int bitpos(string $key, int $bit, int $start, int $end) Find first bit set or clear in a string
 * @method int bitop() @todo
 * @method int decr(string $key) Decrement the integer value of a key by one
 * @method int decrby(string $key, int $decrement) Decrement the integer value of a key by the given number
 * @method string get(string $key) Get the value of a key
 * @method int getbit(string $key, int $offset) Returns the bit value at offset in the string value stored at key
 * @method string getrange(string $key, int $start, int $end) Get a substring of the string stored at a key
 * @method string|null getset(string $key, mixed $value) Set the string value of a key and return its old value
 * @method int incr(string $key) Increment the integer value of a key by one
 * @method int incrby(string $key, int $increment) Increment the integer value of a key by the given amount
 * @method float incrbyfloat(string $key, float $increment)
 * @method array mget(string $key0, string $key1, string $keyN) Get the values of all the given keys
 * @method bool mset(array $keyValuePairs) Set multiple keys to multiple values
 * @method bool msetnx(array $keyValuePairs) Set multiple keys to multiple values, only if none of the keys exist
 * @method bool psetex(string $key, float $milliseconds, mixed $value) Set the value and expiration in milliseconds of a key
 * @method bool set(string $key, string $value, int $expireTime, bool $onlyExist = false) @todo
 * @method int setbit(string $key, int $offset, int $value) Sets or clears the bit at offset in the string value stored at key
 * @method bool setex(string $key, int $seconds, mixed $value) Set the value and expiration of a key
 * @method bool setnx(string $key, mixed $value) Set the value of a key, only if the key does not exist
 * @method int setrange(string $key, int $offset, string $value) Overwrite part of a string at key starting at the specified offset
 * @method int strlen(string $key) Get the length of the value stored in a key
 *
 * @link http://redis.io/commands#server Server
 * @method bool bgrewriteaof() Asynchronously rewrite the append-only file
 * @method bool bgsave() Asynchronously save the dataset to diskAsynchronously save the dataset to disk
 * @method client(string $subCommand, mixed $param0 = null, mixed $paramN = null) @todo
 * @method config(string $subCommand, string $parameter = null, string $value = null) @todo
 * @method int dbsize() Return the number of keys in the currently-selected database.
 * @method debug() @todo
 * @method bool flushall() Remove all keys from all databases. This command never fails.
 * @method bool flushdb() Delete all the keys of the currently selected DB. This command never fails.
 * @method array info(string $section) Get information and statistics about the server
 * @method int lastsave() Return the UNIX TIME of the last DB save executed with success.
 * @method monitor() @todo
 * @method bool save() Synchronously save the dataset to disk
 * @method bool shutdown(bool $save) Synchronously save the dataset to disk and then shut down the server.
 * @method bool slaveof(string $host, int $port) Make the server a slave of another instance, or promote it as master
 * @method slowlog() @todo
 * @method sync() @todo
 * @method array time() Return the current server time. A multi bulk reply containing two elements: unix time in seconds and microseconds.
 *
 * @link http://redis.io/commands#hash Hashes
 * @method int hdel(string $key, string $field0, string $field1, string $fieldN) Delete one or more hash fields
 * @method bool hexists(string $key, string $field) Determine if a hash field exists
 * @method mixed hget(string $key, string $field) Get the value of a hash field
 * @method array hgetall(string $key) Get all the fields and values in a hash
 * @method int hincrby(string $key, string $field, int $increment) Increment the integer value of a hash field by the given number
 * @method float hincrbyfloat(string $key, string $field, float $increment) Increment the float value of a hash field by the given amount
 * @method array hkeys(string $key) Get all the fields in a hash
 * @method int hlen(string $key) Get the number of fields in a hash
 * @method array hmget(string $key, string $field0, string $field1, string $fieldN) Get the values of all the given hash fields
 * @method bool hmset(string $key, array $keyValuePairs) Set multiple hash fields to multiple values
 * @method bool hset(string $key, string $field, mixed $value) Set the string value of a hash field
 * @method bool hsetnx(string $key, string $field, mixed $value) Set the value of a hash field, only if the field does not exist
 * @method array hvals(string $key) Get all the values in a hash
 * @method hscan() @todo
 *
 * @link http://redis.io/commands#list lists
 * @method array blpop(string $key0, string $key1, string $keyN, int $timeout) Remove and get the first element in a list, or block until one is available
 * @method array brpop(string $key0, string $key1, string $keyN, int $timeout) Remove and get the last element in a list, or block until one is available
 * @method mixed brpoplpush(string $sourcekey, string $destinationKey, int $timeout) Pop a value from a list, push it to another list and return it; or block until one is available
 * @method mixed lindex(string $key, int $index) Get an element from a list by its index
 * @method int linsert(string $key, string $position, mixed $pivot, mixed $value) Insert an element before or after another element in a list
 * @method int llen(string $key) Get the length of a list
 * @method mixed lpop(string $key) Remove and get the first element in a list
 * @method int lpush(string $key, mixed $value0, mixed $value1, mixed $valueN) Prepend one or multiple values to a list
 * @method int lpushx(string $key, mixed $value) Prepend a value to a list, only if the list exists
 * @method array lrange(string $key, int $start, int $stop) Get a range of elements from a list
 * @method int lrem(string $key, int $count, mixed $value) Remove elements from a list
 * @method bool lset(string $key, int $index, mixed $value) Set the value of an element in a list by its index
 * @method bool ltrim(string $key, int $start, int $stop) Trim a list to the specified range
 * @method mixed rpop(string $key) Remove and get the last element in a list
 * @method mixed rpoplpush(string $sourcekey, string $destinationKey) Remove the last element in a list, append it to another list and return it
 * @method int rpush(string $key, mixed $value0, mixed $value1, mixed $valueN) Append one or multiple values to a list
 * @method int rpushx(string $key, mixed $value) Append a value to a list, only if the list exists
 *
 * @link http://redis.io/commands#set Sets
 * @method int sadd(string $key, mixed $member0, mixed $member1, mixed $memberN) Add one or more members to a set
 * @method int scard(string $key) Get the number of members in a set
 * @method array sdiff (string $key0, string $key1, string $keyN) Returns the members of the set resulting from the difference between the first set and all the successive sets.
 * @method int sdiffstore (string $destination, string $key0, string $key1, string $keyN) Subtract multiple sets and store the resulting set in a key
 * @method array sinter (string $key0, string $key1, string $keyN) Intersect multiple sets
 * @method int sinterstore (string $destination, string $key0, string $key1, string $keyN) Intersect multiple sets and store the resulting set in a key
 * @method bool sismember(string $key, mixed $member) Determine if a given value is a member of a set
 * @method array smembers(string $key) Returns all the members of the set value stored at key
 * @method bool smove(string $source, string $destination, mixed $member) Move a member from one set to another
 * @method mixed spop(string $key) Remove and return a random member from a set
 * @method mixed srandmember(string $key, int $count) Get one or multiple random members from a set
 * @method int srem(string $key, mixed $member0, mixed $member1, mixed $memberN) Remove one or more members from a set
 * @method array sunion(string $key0, string $key1, string $keyN) Returns the members of the set resulting from the union of all the given sets
 * @method int sunionstore(string $destination, string $key0, string $key1, string $keyN) This command is equal to SUNION, but instead of returning the resulting set, it is stored in destination
 * @method sscan() @todo
 *
 * @link http://redis.io/commands#sorted_set Sorted Sets
 * @method int zadd(string $key, double $score0, mixed $member0, double $scoreN, mixed $memberN) Adds all the specified members with the specified scores to the sorted set stored at key
 * @method int zcard(string $key) Returns the sorted set cardinality (number of elements) of the sorted set stored at key
 * @method int zcount(string $key, double $minScore, double $maxScore) Returns the number of elements in the sorted set at key with a score between min and max
 * @method double zincrby(string $key, double $increment, mixed $member) Increments the score of member in the sorted set stored at key by increment
 * @method zinterstore() @todo
 * @method int zlexcount(string $key, mixed $min, mixed $max) Count the number of members in a sorted set between a given lexicographical range
 * @method array zrange(string $key, int $startIndex, int $stopIndex, bool $withScores) Returns the specified range of elements in the sorted set stored at key
 * @method zrangebylex() @todo
 * @method zrangebyscore() @todo
 * @method int zrank(string $key, mixed $member) Returns the rank of member in the sorted set stored at key, with the scores ordered from low to high
 * @method int zrem(string $key, mixed $member0, mixed $member1, mixed $memberN) Removes the specified members from the sorted set stored at key
 * @method int zremrangebylex(string $key, mixed $min, mixed $max) Remove all members in a sorted set between the given lexicographical range
 * @method int zremrangebyrank(string $key, int $startIndex, int $stopIndex) Remove all members in a sorted set within the given indexes
 * @method int zremrangebyscore(string $key, double $minScore, double $maxScore) Remove all members in a sorted set within the given scores
 * @method array zrevrange(string $key, int $startIndex, int $stopIndex, bool $withScores) Return a range of members in a sorted set, by index, with scores ordered from high to low
 * @method zrevrangebyscore() @todo
 * @method int zrevrank(string $key, mixed $member) Determine the index of a member in a sorted set, with scores ordered from high to low
 * @method double zscore(string $key, mixed $member) Returns the score of member in the sorted set at key
 * @method zunionstore() @todo
 * @method zscan() @todo
 *
 * @link http://redis.io/commands#hyperloglog HyperLogLog
 * @method bool pfadd(string $key, mixed $element0, mixed $element1, mixed $elementN) Adds the specified elements to the specified HyperLogLog
 * @method int pfcount(string $key0, string $key1, string $keyN) Return the approximated cardinality of the set(s) observed by the HyperLogLog at key(s)
 * @method bool pfmerge(string $destinationKey, string $sourceKey0, string $sourceKey1, string $sourceKeyN) Merge N different HyperLogLogs into a single one
 *
 * @link http://redis.io/commands#pubsub Pub/Sub Commands
 * @link http://redis.io/topics/pubsub Pub/Sub
 * @todo check
 * @method mixed psubscribe(string $pattern0, string $pattern1, string $patternN) Subscribes the client to the given patterns.
 * @method int publish(string $channel, string $message) Posts a message to the given channel
 * @method mixed pubsub() @todo
 * @method mixed punsubscribe(string $pattern0, string $pattern1, string $patternN) Unsubscribes the client from the given patterns, or from all of them if none is given
 * @method mixed subscribe(string $channel0, string $channel1, string $channelN) Subscribes the client to the specified channels
 * @method mixed UNSUBSCRIBE(string $channel0, string $channel1, string $channelN)
 */
class Client {

    const FETCH_MODE_OBJECT = 'object';
    const FETCH_MODE_ARRAY  = 'array';
    const FETCH_MODE_RAW    = 'raw';
    const POSITION_BEFORE   = 'BEFORE';
    const POSITION_AFTER    = 'AFTER';

    const COMMAND_CLIENT_GETNAME = 'GETNAME';
    const COMMAND_CLIENT_KILL    = 'KILL';
    const COMMAND_CLIENT_LIST    = 'LIST';
    const COMMAND_CLIENT_PAUSE   = 'PAUSE';
    const COMMAND_CLIENT_SETNAME = 'SETNAME';

    const COMMAND_INFO_SERVER       = 'server';
    const COMMAND_INFO_CLIENTS      = 'clients';
    const COMMAND_INFO_MEMORY       = 'memory';
    const COMMAND_INFO_PERSISTENCE  = 'persistence';
    const COMMAND_INFO_STATS        = 'stats';
    const COMMAND_INFO_REPLICATION  = 'replication';
    const COMMAND_INFO_CPU          = 'cpu';
    const COMMAND_INFO_COMMANDSTATS = 'commandstats';
    const COMMAND_INFO_CLUSTER      = 'cluster';
    const COMMAND_INFO_KEYSPACE     = 'keyspace';
    const COMMAND_INFO_ALL          = 'all';
    const COMMAND_INFO_DEFAULT      = 'default';

    const COMMAND_CONFIG_GET       = 'GET';
    const COMMAND_CONFIG_RESETSTAT = 'RESETSTAT';
    const COMMAND_CONFIG_REWRITE   = 'REWRITE';
    const COMMAND_CONFIG_SET       = 'SET';

    const COMMAND_MIGRATE_COPY    = 'COPY';
    const COMMAND_MIGRATE_REPLACE = 'REPLACE';

    const COMMAND_SET_NX = 'NX';
    const COMMAND_SET_XX = 'XX';

    const COMMAND_ZRANGE_WITHSCORES = 'WITHSCORES';

    const COMMAND_OBJECT_REFCOUNT = 'REFCOUNT';
    const COMMAND_OBJECT_IDLETIME = 'IDLETIME';
    const COMMAND_OBJECT_ENCODING = 'ENCODING';

    protected $responseFetchMode = self::FETCH_MODE_ARRAY;

    /**
     * @var \Redis\Connection\ConnectionInterface
     */
    protected $connection = null;

    /**
     * Constructor
     *
     * @param array $options connection options
     */
    public final function __construct(array $options) {
        $this->setConnection($options);
    }

    /**
     * Closes the opened connection
     *
     * @return $this
     */
    public function close() {
        if (true === $this->isConnected()) {
            if ($this->quit() and $this->getConnection()->close()) {
                $this->connection = null;
            }
        }

        return !$this->isConnected();
    }

    /**
     * Connect to the redis.
     *
     * @return bool
     * @throws \Redis\Connection\Exception
     */
    public function connect() {
        if (false === $this->isConnected()) {
            $this->getConnection()->connect();
        }

        return true;
    }

    /**
     * Returns _TRUE_ if the connection is active.
     *
     * @return bool
     */
    public function isConnected() {
        return ($this->getConnection() and $this->getConnection()->isConnected());
    }

    /**
     * Destructor
     *
     * If the connection is active then will be closed.
     */
    public function __destruct() {
        if ($this->isConnected()) {
            $this->close();
        }
    }

    /**
     * SReturns response fetch mode.
     *
     * @return string
     */
    public function getResponseFetchMode() {
        return $this->responseFetchMode;
    }

    /**
     * Set up response fetch mode.
     *
     * If it is equal FETCH_MODE_OBJECT then each requests returns a \Redis\Response object.
     * If it is equal FETCH_MODE_RAW then each requests returns a raw response (RESP) string.
     * Otherwise, back to primitive values: string, number, boolean or array.
     *
     * @param string $responseFetchMode
     *
     * @return $this
     */
    public function setResponseFetchMode($responseFetchMode) {
        $this->responseFetchMode = $responseFetchMode;

        return $this;
    }

    /**
     * Magice function.
     *
     * This function handle available redis commands.
     *
     * @see Command::COMMAND_<command name>
     *
     * @param string $name redis command name
     * @param array  $args redis command arguments
     *
     * @return Response
     * @throws \Redis\Response\Exception
     * @throws CommandException
     */
    public function __call($name, array $args) {
        if (false === Command::isExists($name)) {
            throw new CommandException('Not supported command (' . strtoupper($name) . ').');
        }

        $command = Command::factory($name, $args);

        if ($command instanceof CommandQueue) {
            $command->setClient($this);

            return $command;
        }

        return $this->sendCommand($command);
    }

    /**
     * Returns redis connection.
     *
     * @return null|\Redis\Connection\ConnectionInterface
     */
    public function getConnection() {
        return $this->connection;
    }

    /**
     * Sets up Redis connection and connect to the database.
     *
     * @param array|ConnectionInterface $connection
     *
     * @return $this
     */
    public function setConnection($connection) {
        if ($connection instanceof ConnectionInterface) {
            $this->connection = $connection;
        } else {
            $this->connection = Connection::factory($connection);
        }

        $this->connect();

        return $this;
    }

    /**
     * Returns a redis client.
     *
     * @param array $options
     *
     * @static
     * @return Client
     */
    public static function create(array $options) {
        return new self($options);
    }

    /**
     * Send the command to the redis and returns response.
     *
     * @param AbstractCommand $command
     *
     * @return Response
     * @throws Response\Exception
     */
    public function sendCommand(AbstractCommand $command) {
        $rawResponse = $this->getConnection()->command($command);
        $response    = Response::create($rawResponse)->setCommand($command);

        if ($response->hasError()) {
            throw $response->getError();
        }

        if (self::FETCH_MODE_OBJECT === $this->getResponseFetchMode()) {
            return $response;
        } elseif (self::FETCH_MODE_RAW === $this->getResponseFetchMode()) {
            return $response->getRawValue();
        }

        return $response->getValue();
    }
}
