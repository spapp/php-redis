<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2014
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   php_redis
 * @since     2014.03.31.
 */
namespace Redis;

use Redis\Command\AbstractCommand;
use Redis\Command\Queue;


/**
 * Class Command
 *
 * @link http://redis.io/topics/data-types
 * @link http://en.wikipedia.org/wiki/Glob_(programming)
 */
class Command {
    const NS                    = '\\Redis\\Command\\';
    const DEFAULT_COMMAND_CLASS = 'DefaultCommand';


//
//    const OPTION_DIRECTION_ASC  = 'ASC';
//    const OPTION_DIRECTION_DESC = 'DESC';
//
//
//    const OPTION_SORT_BY           = 'by';
//    const OPTION_SORT_LIMIT_OFFSET = 'limitOffset';
//    const OPTION_SORT_LIMIT_COUNT  = 'limitCount';
//    const OPTION_SORT_GET          = 'get';
//    const OPTION_SORT_DIRECTION    = 'direction';
//    const OPTION_SORT_ALPHA        = 'alpha';
//    const OPTION_SORT_STORE        = 'store';
//


    /**
     * @link http://redis.io/commands#connection Connection
     */
    const COMMAND_PING   = 'PING';
    const COMMAND_QUIT   = 'QUIT';
    const COMMAND_ECHO   = 'ECHO';
    const COMMAND_SELECT = 'SELECT';
    const COMMAND_AUTH   = 'AUTH';

    /**
     * @link http://redis.io/commands#generic Keys
     */
    const COMMAND_EXISTS    = 'EXISTS';
    const COMMAND_KEYS      = 'KEYS';
    const COMMAND_DEL       = 'DEL';
    const COMMAND_DUMP      = 'DUMP';
    const COMMAND_EXPIRE    = 'EXPIRE';
    const COMMAND_EXPIREAT  = 'EXPIREAT';
    const COMMAND_MOVE      = 'MOVE';
    const COMMAND_OBJECT    = 'OBJECT';
    const COMMAND_PERSIST   = 'PERSIST';
    const COMMAND_PEXPIRE   = 'PEXPIRE';
    const COMMAND_PEXPIREAT = 'PEXPIREAT';
    const COMMAND_PTTL      = 'PTTL';
    const COMMAND_RANDOMKEY = 'RANDOMKEY';
    const COMMAND_RENAME    = 'RENAME';
    const COMMAND_RENAMENX  = 'RENAMENX';
    const COMMAND_RESTORE   = 'RESTORE';
    const COMMAND_SCAN      = 'SCAN';
    const COMMAND_TTL       = 'TTL';
    const COMMAND_TYPE      = 'TYPE';
    const COMMAND_MIGRATE   = 'MIGRATE';
//    const COMMAND_SORT      = 'SORT';

    /**
     * @link http://redis.io/commands#server Server
     */
    const COMMAND_BGREWRITEAOF = 'BGREWRITEAOF';
    const COMMAND_BGSAVE       = 'BGSAVE';
    const COMMAND_DBSIZE       = 'DBSIZE';
    const COMMAND_FLUSHALL     = 'FLUSHALL';
    const COMMAND_FLUSHDB      = 'FLUSHDB';
    const COMMAND_INFO         = 'INFO';
    const COMMAND_LASTSAVE     = 'LASTSAVE';
    const COMMAND_SAVE         = 'SAVE';
    const COMMAND_SHUTDOWN     = 'SHUTDOWN';
    const COMMAND_SLAVEOF      = 'SLAVEOF';
    const COMMAND_TIME         = 'TIME';
    const COMMAND_CLIENT       = 'CLIENT';
    const COMMAND_CONFIG       = 'CONFIG';
//    const COMMAND_DEBUG_OBJECT     = 'DEBUG OBJECT';
//    const COMMAND_DEBUG_SEGFAULT   = 'DEBUG SEGFAULT';
//    const COMMAND_MONITOR          = 'MONITOR';
//    const COMMAND_SLOWLOG          = 'SLOWLOG';
//    const COMMAND_SYNC             = 'SYNC';

    /**
     * @link http://redis.io/commands#string String
     */
    const COMMAND_APPEND      = 'APPEND';
    const COMMAND_BITCOUNT    = 'BITCOUNT';
    const COMMAND_BITPOS      = 'BITPOS';
    const COMMAND_DECR        = 'DECR';
    const COMMAND_DECRBY      = 'DECRBY';
    const COMMAND_GET         = 'GET';
    const COMMAND_GETBIT      = 'GETBIT';
    const COMMAND_GETRANGE    = 'GETRANGE';
    const COMMAND_GETSET      = 'GETSET';
    const COMMAND_INCR        = 'INCR';
    const COMMAND_INCRBY      = 'INCRBY';
    const COMMAND_INCRBYFLOAT = 'INCRBYFLOAT';
    const COMMAND_MGET        = 'MGET';
    const COMMAND_MSET        = 'MSET';
    const COMMAND_MSETNX      = 'MSETNX';
    const COMMAND_PSETEX      = 'PSETEX';
    const COMMAND_SET         = 'SET';
    const COMMAND_SETBIT      = 'SETBIT';
    const COMMAND_SETEX       = 'SETEX';
    const COMMAND_SETNX       = 'SETNX';
    const COMMAND_SETRANGE    = 'SETRANGE';
    const COMMAND_STRLEN      = 'STRLEN';
//    const COMMAND_BITOP       = 'BITOP';

    /**
     * @link http://redis.io/commands#hash Hashes
     */
    const COMMAND_HDEL         = 'HDEL';
    const COMMAND_HEXISTS      = 'HEXISTS';
    const COMMAND_HGET         = 'HGET';
    const COMMAND_HGETALL      = 'HGETALL';
    const COMMAND_HINCRBY      = 'HINCRBY';
    const COMMAND_HINCRBYFLOAT = 'HINCRBYFLOAT';
    const COMMAND_HKEYS        = 'HKEYS';
    const COMMAND_HLEN         = 'HLEN';
    const COMMAND_HMGET        = 'HMGET';
    const COMMAND_HMSET        = 'HMSET';
    const COMMAND_HSET         = 'HSET';
    const COMMAND_HSETNX       = 'HSETNX';
    const COMMAND_HVALS        = 'HVALS';
//    const COMMAND_HSCAN        = 'HSCAN';

    /**
     * @link http://redis.io/commands#list Lists
     */
    const COMMAND_BLPOP      = 'BLPOP';
    const COMMAND_BRPOP      = 'BRPOP';
    const COMMAND_BRPOPLPUSH = 'BRPOPLPUSH';
    const COMMAND_LINDEX     = 'LINDEX';
    const COMMAND_LINSERT    = 'LINSERT';
    const COMMAND_LLEN       = 'LLEN';
    const COMMAND_LPOP       = 'LPOP';
    const COMMAND_LPUSH      = 'LPUSH';
    const COMMAND_LPUSHX     = 'LPUSHX';
    const COMMAND_LRANGE     = 'LRANGE';
    const COMMAND_LREM       = 'LREM';
    const COMMAND_LSET       = 'LSET';
    const COMMAND_LTRIM      = 'LTRIM';
    const COMMAND_RPOP       = 'RPOP';
    const COMMAND_RPOPLPUSH  = 'RPOPLPUSH';
    const COMMAND_RPUSH      = 'RPUSH';
    const COMMAND_RPUSHX     = 'RPUSHX';

    /**
     * @link http://redis.io/commands#set Sets
     */
    const COMMAND_SADD        = 'SADD';
    const COMMAND_SCARD       = 'SCARD';
    const COMMAND_SDIFF       = 'SDIFF';
    const COMMAND_SDIFFSTORE  = 'SDIFFSTORE';
    const COMMAND_SINTER      = 'SINTER';
    const COMMAND_SINTERSTORE = 'SINTERSTORE';
    const COMMAND_SISMEMBER   = 'SISMEMBER';
    const COMMAND_SMEMBERS    = 'SMEMBERS';
    const COMMAND_SMOVE       = 'SMOVE';
    const COMMAND_SPOP        = 'SPOP';
    const COMMAND_SRANDMEMBER = 'SRANDMEMBER';
    const COMMAND_SREM        = 'SREM';
    const COMMAND_SUNION      = 'SUNION';
    const COMMAND_SUNIONSTORE = 'SUNIONSTORE';
//    const COMMAND_SSCAN       = 'SSCAN';

    /**
     * @link http://redis.io/commands#sorted_set Stored Sets
     */
    const COMMAND_ZADD             = 'ZADD';
    const COMMAND_ZCARD            = 'ZCARD';
    const COMMAND_ZCOUNT           = 'ZCOUNT';
    const COMMAND_ZINCRBY          = 'ZINCRBY';
    const COMMAND_ZLEXCOUNT        = 'ZLEXCOUNT';
    const COMMAND_ZRANGE           = 'ZRANGE';
    const COMMAND_ZRANK            = 'ZRANK';
    const COMMAND_ZREM             = 'ZREM';
    const COMMAND_ZREMRANGEBYLEX   = 'ZREMRANGEBYLEX';
    const COMMAND_ZREMRANGEBYRANK  = 'ZREMRANGEBYRANK';
    const COMMAND_ZREMRANGEBYSCORE = 'ZREMRANGEBYSCORE';
    const COMMAND_ZREVRANGE        = 'ZREVRANGE';
    const COMMAND_ZREVRANK         = 'ZREVRANK';
    const COMMAND_ZSCORE           = 'ZSCORE';
//    const COMMAND_ZINTERSTORE      = 'ZINTERSTORE';
//    const COMMAND_ZRANGEBYLEX      = 'ZRANGEBYLEX';
//    const COMMAND_ZRANGEBYSCORE    = 'ZRANGEBYSCORE';
//    const COMMAND_ZREVRANGEBYSCORE = 'ZREVRANGEBYSCORE';
//    const COMMAND_ZSCAN            = 'ZSCAN';
//    const COMMAND_ZUNIONSTORE      = 'ZUNIONSTORE';

    /**
     * @link http://redis.io/commands#hyperloglog
     */
    const COMMAND_PFADD   = 'PFADD';
    const COMMAND_PFCOUNT = 'PFCOUNT';
    const COMMAND_PFMERGE = 'PFMERGE';

    /**
     * @link http://redis.io/commands#pubsub Pub/Sub
     */
    const COMMAND_PSUBSCRIBE   = 'PSUBSCRIBE';
    const COMMAND_PUBLISH      = 'PUBLISH';
    const COMMAND_PUNSUBSCRIBE = 'PUNSUBSCRIBE';
    const COMMAND_SUBSCRIBE    = 'SUBSCRIBE';
    const COMMAND_UNSUBSCRIBE  = 'UNSUBSCRIBE';
//    const COMMAND_PUBSUB       = 'PUBSUB';


    protected static $commandClasses = array();

    /**
     * Returns _TRUE_ if the command is an existing (implemented) command.
     *
     * @param string $command
     *
     * @static
     * @return bool
     */
    public static function isExists($command) {
        return defined('self::COMMAND_' . strtoupper($command));
    }

    /**
     * @param string $name
     * @param array  $arguments
     *
     * @static
     * @return AbstractCommand|Queue
     */
    public static function factory($name, array $arguments = array()) {
        $name = ucfirst(strtolower($name));

        if (in_array(strtoupper($name), self::$commandClasses, true)) {
            $className = self::NS . $name;
            $instance  = new $className();
        } else {
            $className = self::NS . self::DEFAULT_COMMAND_CLASS;
            $instance  = new $className($name);
        }

        $instance->setArguments($arguments);

        return $instance;
    }
}
