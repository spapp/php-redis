<?php
/**
 * @author     Sándor Papp <spapp@spappsite.hu>
 * @copyright  2014
 * @license    http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package    php_redis
 * @subpackage Redis\Command
 * @since      2014.04.30.
 */

namespace Redis\Command;

use Redis\Command;
use Redis\Command\Filter;
use Redis\Filter\FilterInterface;
use Redis\Resp\Command as RespCommand;

/**
 * Class AbstractCommand
 */
class AbstractCommand implements CommandInterface {

    /**
     * @var RespCommand
     */
    protected $command = null;

    /**
     * @var FilterInterface
     */
    protected $filter = null;

    /**
     * @var array
     * @static
     * @todo
     */
    protected static $filters = array(
            Command::COMMAND_SCAN      => 'scan',
            //            Command::COMMAND_SORT     => 'sort',
            Command::COMMAND_SHUTDOWN  => 'shutdown',
            Command::COMMAND_SET       => 'set',
            Command::COMMAND_MSET      => 'mset',
            Command::COMMAND_MSETNX    => 'mset',
            Command::COMMAND_HMSET     => 'hmset',
            Command::COMMAND_ZADD      => 'zadd',
            Command::COMMAND_ZRANGE    => 'zrange',
            Command::COMMAND_ZREVRANGE => 'zrevrange',
            Command::COMMAND_CLIENT    => 'client'
    );

    /**
     * Constructor
     *
     * @param string $command command name
     */
    public function __construct($command) {
        $this->setCommand(RespCommand::create($command));
        $this->setFilter();
    }

    /**
     * Set up command name
     *
     * @param string $commandName
     *
     * @return $this
     */
    public function setName($commandName) {
        $this->getCommand()->setName($commandName);

        return $this;
    }

    /**
     * Set up command arguments.
     *
     * @param array $arguments
     *
     * @return $this
     */
    public function setArguments(array $arguments) {
        if ($this->hasFilter()) {
            $arguments = $this->getFilter()->filter($arguments);
        }

        $this->getCommand()->addArgument($arguments);

        return $this;
    }

    /**
     * Returns command arguments.
     *
     * @return array
     */
    public function getArguments() {
        return $this->getCommand()->getArguments();
    }

    /**
     * Set up a response filter.
     *
     * @param FilterInterface $filter
     *
     * @todo
     *
     * @return $this
     */
    public function setFilter(FilterInterface $filter = null) {
        if (null === $filter and array_key_exists($this->getCommand()->getName(), self::$filters)) {
            $filter = Filter::factory(self::$filters[$this->getCommand()->getName()]);
        }

        $this->filter = $filter;

        return $this;
    }

    /**
     * Returns a response filter.
     *
     * @todo
     * @return FilterInterface
     */
    public function getFilter() {
        return $this->filter;
    }

    /**
     * Returns _TRUE_ if filter is exists and it is implements filter interface.
     *
     * @return bool
     */
    public function hasFilter() {
        return ($this->filter and $this->filter instanceof FilterInterface);
    }

    /**
     * Magice function.
     *
     * Returns command as a resp string.
     *
     * @return string
     */
    public function __toString() {
        return $this->getCommand()->toString();
    }

    /**
     * Returns command name
     *
     * @return string
     */
    public function getName() {
        return $this->getCommand()->getName();
    }

    /**
     * Set up resp commnad.
     *
     * @param RespCommand $command
     *
     * @return $this
     */
    protected function setCommand(RespCommand $command) {
        $this->command = $command;

        return $this;
    }

    /**
     * Returns resp command.
     *
     * @return RespCommand
     */
    protected function getCommand() {
        return $this->command;
    }
} 