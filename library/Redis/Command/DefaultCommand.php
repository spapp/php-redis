<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2014
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   php_redis
 * @subpackage Redis\Command
 * @since     2014.04.30.
 */

namespace Redis\Command;

/**
 * Class DefaultCommand
 */
class DefaultCommand extends AbstractCommand {

} 