<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2014
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   php_redis
 * @since     2014.04.14.
 */
namespace Redis\Command;

/**
 * Class Filter
 */
class Filter {
    const NS = '\\Redis\\Command\\Filter\\';

    /**
     * Constructor
     */
    public final function __construct() {

    }

    /**
     * Returns a command filter.
     *
     * @param array|string $name
     * @param null|array   $options
     *
     * @static
     * @return \Redis\Filter\FilterInterface
     */
    public static function factory($name, $options = null) {
        if (is_array($name)) {
            $options = $name['options'];
            $name    = $name['class'];
        }

        if (false === strpos($name, self::NS)) {
            $name = self::NS . ucfirst($name);
        }

        return new $name($options);
    }
}
