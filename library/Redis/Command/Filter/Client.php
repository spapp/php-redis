<?php
/**
 * @author     Sándor Papp <spapp@spappsite.hu>
 * @copyright  2014
 * @license    http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package    php_redis
 * @subpackage Redis\Command\Filter
 * @since      2014.06.12.
 */

namespace Redis\Command\Filter;

use Redis\Client as RedisClient;
use Redis\Filter\FilterInterface;

/**
 * Class Client
 */
class Client implements FilterInterface {
    /**
     * Constructor
     */
    public function __construct() {
    }

    /**
     * Prepare arguments for Redis.
     *
     * @param array $value command arguments
     *
     * @return array param list for redis command
     */
    public function filter($value) {
        $return = array(strtoupper($value[0]));

        switch ($return[0]) {
            case RedisClient::COMMAND_CLIENT_KILL:
                $address = $value[1];

                if (isset($value[2])) {
                    $address .= ':' . $value[2];
                }

                array_push($return, $address);
                break;
            case RedisClient::COMMAND_CLIENT_PAUSE:
            case RedisClient::COMMAND_CLIENT_SETNAME:
                array_push($return, $value[1]);
                break;
            case RedisClient::COMMAND_CLIENT_GETNAME:
            case RedisClient::COMMAND_CLIENT_LIST:
                break;
        }

        return $return;
    }
}
