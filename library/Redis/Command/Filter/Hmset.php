<?php
/**
 * @author     Sándor Papp <spapp@spappsite.hu>
 * @copyright  2014
 * @license    http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package    php_redis
 * @subpackage Redis\Command\Filter
 * @since      2014.05.22.
 */

namespace Redis\Command\Filter;

use Redis\Filter\FilterInterface;

/**
 * Class Hmset
 */
class Hmset implements FilterInterface {
    /**
     * Constructor
     */
    public function __construct() {
    }

    /**
     * Prepare arguments for Redis.
     *
     * @param array $value command arguments
     *
     * @return array param list for redis command
     */
    public function filter($value) {
        $return = array($value[0]);

        if (2 === count($value) and is_array($value[1])) {
            foreach ($value[1] as $key => $value) {
                array_push($return, $key, $value);
            }
        } else {
            $return = $value;
        }

        return $return;
    }
} 