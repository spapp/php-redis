<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2014
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   php_redis
 * @since     2014.04.14.
 */
namespace Redis\Command\Filter;

use Redis\Filter\FilterInterface;

/**
 * Class Redis\Command\Filter\Scan
 */
class Scan implements FilterInterface {
    /**
     * Constructor
     */
    public function __construct() {
    }

    /**
     * Prepare arguments for Redis.
     *
     * @param array $value command arguments
     *
     * @return array param list for redis command
     */
    public function filter($value) {
        $return = array($value[0]);

        if (isset($value[1])) {
            array_push($return, 'MATCH', $value[1]);
        }

        if (isset($value[2])) {
            array_push($return, 'COUNT', $value[2]);
        }

        return $return;
    }
}
