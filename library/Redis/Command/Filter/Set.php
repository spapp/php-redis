<?php
/**
 * @author     Sándor Papp <spapp@spappsite.hu>
 * @copyright  2014
 * @license    http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package    php_redis
 * @subpackage Redis\Command\Filter
 * @since      2014.06.13.
 */

namespace Redis\Command\Filter;

use Redis\Filter\FilterInterface;

/**
 * Class Set
 */
class Set implements FilterInterface {
    /**
     * Constructor
     */
    public function __construct() {
    }

    /**
     * Prepare arguments for Redis.
     *
     * @param array $value command arguments
     *
     * @return array param list for redis command
     */
    public function filter($value) {
        $return = array($value[0], $value[1]);

        if (isset($value[2])) {
            array_push($return, 'EX', ceil($value[2]));
        }

        if (isset($value[3])) {
            if (true === $value[3]) {
                array_push($return, 'XX');
            } else {
                array_push($return, 'NX');
            }
        }

        return $return;
    }
} 