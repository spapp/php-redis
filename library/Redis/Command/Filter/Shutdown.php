<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2014
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   php_redis
 * @since     2014.05.16.
 */
namespace Redis\Command\Filter;

use Redis\Command;
use Redis\Filter\FilterInterface;

/**
 * Class Redis\Command\Filter\Shutdown
 */
class Shutdown implements FilterInterface {
    const OPTION_NOSAVE = 'NOSAVE';
    const OPTION_SAVE   = 'SAVE';

    /**
     * Constructor
     */
    public function __construct() {
    }

    /**
     * Prepare arguments for Redis.
     *
     * @param array $value
     *
     * @return string
     */
    public function filter($value) {
        $return = self::OPTION_NOSAVE;

        if (isset($value[0]) and true === $value[0]) {
            $return = self::OPTION_SAVE;
        }

        return $return;
    }
}
