<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2014
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   php_redis
 * @since     2014.04.14.
 */
namespace Redis\Command\Filter;

use Redis\Command;
use Redis\Filter\FilterInterface;

/**
 * Class Redis\Command\Filter\Sort
 */
class Sort implements FilterInterface {
    /**
     * Constructor
     */
    public function __construct() {
    }

    /**
     * Prepare arguments for Redis.
     *
     * options
     *      ['by']          string  a pattern to generate the keys
     *      ['limitOffset'] int
     *      ['limitCount']  int
     *      ['get']         array   patterns
     *      ['direction']   string  ASC|DESC
     *      ['alpha']       bool
     *      ['store']       string
     *
     * @param array $value the first element is the key and the secund element options that contains some property (see above)
     *
     * @return array arguments list
     */
    public function filter($value) {
        if (isset($value[1])) {
            $options = $value[1];
        } else {
            $options = array();
        }

        $return = array($value[0]);

        if (isset($options[Command::OPTION_SORT_BY])) {
            array_push($return, 'BY', $options[Command::OPTION_SORT_BY]);
        }

        if (isset($options[Command::OPTION_SORT_LIMIT_OFFSET]) and
            isset($options[Command::OPTION_SORT_LIMIT_COUNT])
        ) {
            array_push($return,
                       'LIMIT',
                       $options[Command::OPTION_SORT_LIMIT_OFFSET],
                       $options[Command::OPTION_SORT_LIMIT_COUNT]
            );
        }

        if (isset($options[Command::OPTION_SORT_GET])) {
            foreach ($options[Command::OPTION_SORT_GET] as $get) {
                array_push($return, 'GET', $get);
            }
        }

        if (isset($options[Command::OPTION_SORT_DIRECTION])) {
            if (Command::OPTION_DIRECTION_DESC === $options[Command::OPTION_SORT_DIRECTION]) {
                array_push($return, Command::OPTION_DIRECTION_DESC);
            } else {
                array_push($return, Command::OPTION_DIRECTION_ASC);
            }
        }

        if (isset($options[Command::OPTION_SORT_ALPHA]) and true === $options[Command::OPTION_SORT_ALPHA]) {
            array_push($return, 'ALPHA');
        }

        if (isset($options[Command::OPTION_SORT_STORE])) {
            array_push($return, 'STORE', $options[Command::OPTION_SORT_STORE]);
        }

        return $return;
    }
}
