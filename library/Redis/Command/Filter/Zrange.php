<?php
/**
 * @author     Sándor Papp <spapp@spappsite.hu>
 * @copyright  2014
 * @license    http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package    php_redis
 * @subpackage Redis\Command\Filter
 * @since      2014.05.23.
 */

namespace Redis\Command\Filter;

use Redis\Client;
use Redis\Command;
use Redis\Filter\FilterInterface;

/**
 * Class Zrange
 */
class Zrange implements FilterInterface {
    /**
     * Constructor
     */
    public function __construct() {
    }

    /**
     * Prepare arguments for Redis.
     *
     * @param array $value command arguments
     *
     * @return array param list for redis command
     */
    public function filter($value) {
        if (4 === count($value)) {
            if (true === $value[3] or 'true' === $value[3]) {
                $value[3] = Client::COMMAND_ZRANGE_WITHSCORES;
            }

            if (Client::COMMAND_ZRANGE_WITHSCORES !== $value[3]) {
                unset($value[3]);
            }
        }

        return $value;
    }
}