<?php
/**
 * @author     Sándor Papp <spapp@spappsite.hu>
 * @copyright  2014
 * @license    http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package    php_redis
 * @subpackage Redis\Command
 * @since      2014.04.30.
 */

namespace Redis\Command;

use Redis\Client;

/**
 * Class Queue
 */
class Queue extends AbstractCommand {
    /**
     * @var string
     */
    protected $commandName = null;

    /**
     * @var Client
     */
    protected $client = null;

    /**
     * Constructor
     */
    public function __construct() {
        parent::__construct($this->commandName);
    }

    /**
     * @return Client
     */
    public function getClient() {
        return $this->client;
    }

    /**
     * @param Client $client
     *
     * @return $this
     */
    public function setClient(Client $client) {
        $this->client = $client;

        return $this;
    }
}
