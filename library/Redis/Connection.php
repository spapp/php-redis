<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2014
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   php_redis
 * @since     2014.04.14.
 */
namespace Redis;

/**
 * Class \Redis\Connection
 */
class Connection {
    const NS_CONNECTION = '\\Redis\\Connection\\';

    const PROTOCOL_TCP  = 'tcp';
    const PROTOCOL_UNIX = 'unix';

    const OPTION_TIMEOUT            = 'timeout';
    const OPTION_CONNECTION_TIMEOUT = 'connectionTimeout';
    const OPTION_PROTOCOL           = 'protocol';
    const OPTION_HOST               = 'host';
    const OPTION_PORT               = 'port';
    const OPTION_PATH               = 'path';
    const OPTION_ASYNC              = 'async';
    const OPTION_PERSIST            = 'persist';

    const DEFAULT_CONNECTION_TIMEOUT = 60;
    const DEFAULT_TIMEOUT            = .01;
    const DEFAULT_PROTOCOL           = self::PROTOCOL_TCP;

    /**
     * Factory a connection to a redis database.
     *
     * @param string $protocol
     * @param array  $options
     *
     * @static
     * @return \Redis\Connection\ConnectionInterface
     */
    public static function factory($protocol, array $options = null) {
        if (is_array($protocol)) {
            $options = $protocol;
        }

        if (isset($options[self::OPTION_PROTOCOL])) {
            $protocol = $options[self::OPTION_PROTOCOL];
            unset($options[self::OPTION_PROTOCOL]);
        } else {
            $protocol = self::DEFAULT_PROTOCOL;
        }

        $className = self::NS_CONNECTION . ucfirst(strtolower($protocol));

        return new $className($options);
    }
}
