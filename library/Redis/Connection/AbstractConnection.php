<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2014
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   php_redis
 * @since     2014.03.31.
 */
namespace Redis\Connection;

use Redis\Connection\Exception as ConnectionException;
use Redis\Connection;
use Redis\Traits\OptionTrait;

/**
 * Class AbstractConnection
 */
abstract class AbstractConnection implements ConnectionInterface {
    use OptionTrait;

    /**
     * @var resource stream resource
     */
    protected $connection = null;

    /**
     * Constructor
     *
     * options:
     *      - timeout               // <seconds>.<microseconds>
     *      - connectionTimeout     // <seconds>
     *      - protocol              // string
     *      - host                  // string
     *      - port                  // int
     *      - path                  // string
     *      - async                 // true|false
     *      - persist               // true|false
     *
     * @see Connection::OPTION_<name>
     *
     * @param array $options
     */
    public function __construct(array $options) {
        $this->setOptions($options);
    }

    /**
     * Connect to the redis.
     *
     * Opens a socket connection.
     * On success a stream resource is set up.
     *
     * @return bool
     * @throws ConnectionException
     */
    public function connect() {
        $this->connection = stream_socket_client(
                $this->getRemoteSocketAddress(),
                $errorNo,
                $errorText,
                $this->getConnectionTimeout(),
                $this->getFlags()
        );

        if ($this->connection) {
            $this->setStreamTimeout();
        } else {
            throw new ConnectionException($errorText, $errorNo);
        }

        return $this->isConnected();
    }

    /**
     * Sets timeout period on a stream.
     *
     * format: <seconds>.<microseconds>
     *
     * @return $this
     */
    protected function setStreamTimeout() {
        $timeout      = $this->getOption(Connection::OPTION_TIMEOUT, Connection::DEFAULT_TIMEOUT);
        $seconds      = floor($timeout);
        $microseconds = ($timeout - $seconds) * 1000000;

        if ($seconds < 1 and $microseconds < 1) {
            $microseconds = 1;
        }

        stream_set_timeout($this->connection, $seconds, ceil($microseconds));

        return $this;
    }

    /**
     * Send a command to database
     *
     * @param string $command
     *
     * @return string
     * @throws ConnectionException
     */
    public function command($command) {
        $maxCount = 10000;
        $this->write((string)$command);

        do {
            $data = $this->read();
        } while (!$data and --$maxCount > 0);

        return $data;
    }

    /**
     * Closes the opened connection.
     *
     * @return bool
     */
    public function close() {
        if (true === $this->isConnected()) {
            fclose($this->connection);
            $this->connection = null;
        }

        return !$this->isConnected();
    }

    /**
     * Returns _TRUE_ if the connection is active.
     *
     * @return bool
     */
    public function isConnected() {
        return (bool)$this->connection;
    }

    /**
     * Reads data from the socket
     *
     * @return string
     */
    protected function read() {
        return stream_get_contents($this->connection, -1);
    }

    /**
     * Writes data to the socket.
     *
     * Sends a command to the redis.
     * Returns _TRUE_ if all data send otherwise returns _FALSE_
     *
     * @param string $data
     *
     * @return bool
     * @throws ConnectionException
     */
    protected function write($data) {
        while ($length = strlen($data)) {
            $written = fwrite($this->connection, $data);

            if (false == $written) {
                $error = error_get_last();
                throw new ConnectionException($error['message'], $error['type']);
            } else {
                $data = substr($data, (int)$written);
            }
        }

        return (strlen($data) < 1);
    }

    /**
     * Returns socket connection timeout.
     *
     * Number of seconds until the connect() system call should timeout.
     *
     * @see http://www.php.net/manual/en/filesystem.configuration.php#ini.default-socket-timeout
     * @see Redis_Connection_Abstract::DEFAULT_CONNECTION_TIMEOUT
     * @return int
     */
    protected function getConnectionTimeout() {
        $defaultTimeout = ini_get('default_socket_timeout');
        $defaultTimeout = $defaultTimeout ? $defaultTimeout : Connection::DEFAULT_CONNECTION_TIMEOUT;

        return $this->getOption(Connection::OPTION_CONNECTION_TIMEOUT, $defaultTimeout);
    }

    /**
     * Returns socket connection flags.
     *
     * Bitmask field which may be set to any combination of connection flags.
     *
     * @return int
     */
    protected function getFlags() {
        $flags = STREAM_CLIENT_CONNECT;

        if (true === $this->getOption(Connection::OPTION_ASYNC, false)) {
            $flags |= STREAM_CLIENT_ASYNC_CONNECT;
        }

        if (true === $this->getOption(Connection::OPTION_PERSIST, false)) {
            $flags |= STREAM_CLIENT_PERSISTENT;
        }

        return $flags;
    }

    /**
     * Return address to the socket to connect to.
     *
     * @return string
     */
    abstract protected function getRemoteSocketAddress();
}
