<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2014
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   php_redis
 * @since     2014.04.14.
 */
namespace Redis\Connection;

/**
 * Class ConnectionInterface
 */
interface ConnectionInterface {
    /**
     * Connect to the redis.
     *
     * @return mixed
     */
    public function connect();

    /**
     * Closes the opened connection.
     *
     * @return mixed
     */
    public function close();

    /**
     * Returns _TRUE_ if the connection is active.
     *
     * @return bool
     */
    public function isConnected();

    /**
     * Send a command to database.
     *
     * @param string $command
     *
     * @return string
     */
    public function command($command);
}
