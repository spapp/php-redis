<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2014
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   php_redis
 * @since     2014.03.31.
 */
namespace Redis\Connection;

use Redis\Connection;

/**
 * Class Tcp
 */
class Tcp extends AbstractConnection {

    const DEFAULT_HOST = '127.0.0.1';
    const DEFAULT_PORT = 6379;

    /**
     * Return address to the socket to connect to.
     *
     * @return string
     */
    protected function getRemoteSocketAddress() {
        $uri = Connection::PROTOCOL_TCP . '://';
        $uri .= $this->getOption('host', self::DEFAULT_HOST);
        $uri .= ':' . $this->getOption('port', self::DEFAULT_PORT);
        $uri .= $this->getOption('path', '');

        return $uri;
    }
}
