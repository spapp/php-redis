<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2014
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   php_redis
 * @since     2014.04.14.
 */
namespace Redis\Filter;

/**
 * Class \Redis\Filter\FilterInterface
 */
interface FilterInterface {
    /**
     * Returns filtered params.
     *
     * @param mixed $params
     *
     * @return mixed
     */
    public function filter($params);
}
