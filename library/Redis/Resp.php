<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2014
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   php_redis
 * @since     2014.04.04.
 */
namespace Redis;

use Redis\Resp\Exception as RespException;

/**
 * Class \Redis\Resp
 *
 * Redis clients communicate with the Redis server using a protocol
 * called RESP (REdis Serialization Protocol).
 *
 * @see http://redis.io/topics/protocol
 */
class Resp {
    const EOL = "\r\n";

    const TYPE_STRING      = '+';
    const TYPE_ERROR       = '-';
    const TYPE_INTEGER     = ':';
    const TYPE_BULK_STRING = '$';
    const TYPE_ARRAY       = '*';

    /**
     * @var array
     */
    protected $value = array();

    /**
     * @var array
     */
    protected $errors = array();

    /**
     * @var array REdis Serialization Protocol type names
     */
    protected $types = array(
            self::TYPE_STRING      => 'string',
            self::TYPE_ERROR       => 'error',
            self::TYPE_INTEGER     => 'integer',
            self::TYPE_BULK_STRING => 'bulkstring',
            self::TYPE_ARRAY       => 'array',
    );

    /**
     * Constructor
     *
     * @param string $value RESP protocol string
     */
    public function __construct($value = null) {
        if (null !== $value) {
            $this->setValue($value);
        }
    }

    /**
     * Returns error messages.
     *
     * @return array
     */
    public function getErrors() {
        return $this->errors;
    }

    /**
     * Sets up RESP protocol string.
     *
     * @param string $value RESP protocol string
     *
     * @return $this
     */
    public function setValue($value) {
        $this->value = explode(self::EOL, (string)$value);

        return $this;
    }

    /**
     * Returns parsed RESP protocol string.
     *
     * The return value deppends on the request.
     *
     * @return string|array
     * @throws RespException
     */
    public function getValue() {
        $data = $this->prepare($this->value);

        if (array_key_exists(0, $data)) {
            $data = $data[0];
        }

        return $data;
    }

    /**
     * Returns a RESP protocol string.
     *
     * @see Resp::add
     * @return string RESP protocol string
     */
    public function toString() {
        return implode(self::EOL, $this->value) . self::EOL;
    }

    /**
     * Magice method.
     *
     * @see Resp::toString
     * @return string RESP protocol string
     */
    public function __toString() {
        return (string)$this->toString();
    }

    /**
     * Adds a new value for the command.
     *
     * @param string $type RESP protocol type string
     * @param mixed  $value
     *
     * @see Resp::add<type name>
     * @return $this
     * @throws RespException
     */
    public function add($type, $value) {

        if (is_null($value)) {
            $this->addNull();
        } elseif (true === $this->isValidType($type)) {
            $funcName = 'add' . ucfirst($this->types[$type]);

            $this->{$funcName}($value);
        } else {
            throw new RespException('Not supported type (' . $type . ').');
        }

        return $this;
    }

    /**
     * Adds a new RESP Null Bulk String value.
     *
     * <pre>
     *  "$-1\r\n"
     * </pre>
     *
     * @return $this
     */
    protected function addNull() {
        array_push($this->value, self::TYPE_BULK_STRING . '-1');

        return $this;
    }

    /**
     * Adds a new RESP Simple String value.
     *
     * <pre>
     *  "+OK\r\n"
     * </pre>
     *
     * @param string $value
     *
     * @return $this
     */
    protected function addString($value) {
        array_push($this->value, self::TYPE_STRING . (string)$value);

        return $this;
    }

    /**
     * Adds a new RESP Error value.
     *
     * <pre>
     *  "-ERR unknown command 'foobar'\r\n-WRONGTYPE Operation against a key holding the wrong kind of value\r\n"
     * </pre>
     *
     * @param string|\Exception $value
     *
     * @return $this
     */
    protected function addError($value) {
        if ($value instanceof \Exception) {
            $value = get_class($value) . ' ' . $value->getMessage();
        }

        array_push($this->value, self::TYPE_ERROR . $value);
        array_push($this->errors, $value); // fixme

        return $this;
    }

    /**
     * Adds a new RESP Integer value.
     *
     * <pre>
     *  ":0\r\n"
     *  ":1000\r\n"
     * </pre>
     *
     * @param int $value
     *
     * @return $this
     */
    protected function addInteger($value) {
        array_push($this->value, self::TYPE_INTEGER . (int)$value);

        return $this;
    }

    /**
     * Adds a new RESP Bulk String value.
     *
     * <pre>
     *  "$6\r\nfoobar\r\n"
     *
     *  // When an empty string is just
     *  "$0\r\n\r\n"
     * </pre>
     *
     * @param string $value
     *
     * @return $this
     */
    protected function addBulkstring($value) {
        array_push($this->value, self::TYPE_BULK_STRING . strlen($value), (string)$value);

        return $this;
    }

    /**
     * Adds a new RESP Bulk String value.
     *
     * <pre>
     *  // empty Array
     *  "*0\r\n"
     *
     *  "*3\r\n:1\r\n:2\r\n:3\r\n"
     * </pre>
     *
     * <pre>
     * $value = array(
     *              array(
     *                  '_type'     => Redis_Resp::TYPE_BULK_STRING,
     *                  '_value'    => Redis_Client::COMMAND_PING
     *              )
     *          );
     * </pre>
     *
     * @param array $value
     *
     * @return $this
     * @throws RespException
     */
    protected function addArray($value) {
        $defaultType = '';

        if (array_key_exists('_type', $value)) {
            $defaultType = $value['_type'];
            $data        = $value['_value'];
        } else {
            $data = $value;
        }

        array_push($this->value, self::TYPE_ARRAY . count($data));

        foreach ($data as $item) {
            if (is_array($item)) {
                if (array_key_exists('_value', $item) and is_array($item['_value'])) {
                    $this->addArray($item);
                } else {
                    if (array_key_exists('_type', $item)) {
                        $type = $item['_type'];
                    } elseif ($defaultType) {
                        $type = $defaultType;
                    } else {
                        $type = $this->getValueType($item['_value']);
                    }

                    $this->add($type, $item['_value']);
                }
            } else {
                if ($defaultType) {
                    $type = $defaultType;
                } else {
                    $type = $this->getValueType($item);
                }

                $this->add($type, $item);
            }
        }

        return $this;
    }

    /**
     * Returns RESP protocol type.
     *
     * @param mixed $value
     *
     * @return string
     */
    protected function getValueType($value) {
        switch (strtolower(gettype($value))) {
            case 'boolean':
            case 'integer':
            case 'double':
                $type = self::TYPE_INTEGER;
                break;
            case 'array':
                $type = self::TYPE_ARRAY;
                break;
            case 'string':
            default:
                $type = self::TYPE_STRING;
        }

        return $type;
    }

    /**
     * Returns _TRUE_ if it is a valid RESP type.
     *
     * @param string $type RESP protocol type
     *
     * @return bool
     */
    protected function isValidType($type) {
        return array_key_exists($type, $this->types);
    }

    /**
     * Prepare RESP protocol string.
     *
     * @param array $data
     *
     * @return array
     * @throws RespException
     */
    protected function prepare(array $data) {
        $return = array();

        while ($line = ltrim(array_shift($data))) {
            $type = substr($line, 0, 1);

            if (true === $this->isValidType($type)) {
                $funcName = 'prepare' . ucfirst($this->types[$type]);
                array_push($return, $this->{$funcName}($line, $data));
            } else {
                throw new RespException('Not supported type (' . $type . ').');
            }
        }

        return $return;
    }

    /**
     * Prepare RESP protocol string type.
     *
     * @param string $value
     * @param array  &$data
     *
     * @return string
     */
    protected function prepareString($value, &$data) {
        return substr(trim($value), 1);
    }

    /**
     * Prepare RESP protocol error type.
     *
     * @param string $value
     * @param array  &$data
     *
     * @return string
     */
    protected function prepareError($value, &$data) {
        $message = substr(trim($value), 1);

        // fixme
        array_push($this->errors, $message);

        return $message;
    }

    /**
     * Prepare RESP protocol integer type.
     *
     * @param string $value
     * @param array  &$data
     *
     * @return int
     */
    protected function prepareInteger($value, &$data) {
        return (int)substr($value, 1);
    }

    /**
     * Prepare RESP protocol bulk string type.
     *
     * @param string $value
     * @param array  &$data
     *
     * @return mixed|null
     */
    protected function prepareBulkstring($value, &$data) {
        $length = (int)substr($value, 1);
        $return = null;

        if ($length > -1) {
            $return = array_shift($data);

            while (strlen($return) < $length) {
                $return .= self::EOL . array_shift($data);
            }
        }

        return $return;
    }

    /**
     * Prepare RESP protocol array type.
     *
     * @param string $value
     * @param array  &$data
     *
     * @return array|null
     * @throws RespException
     */
    protected function prepareArray($value, &$data) {
        $length = (int)substr($value, 1);
        $tmp    = array();
        $return = null;

        if ($length > 0) {
            for ($i = 0; $i < $length; $i++) {
                $line = array_shift($data);

                if (self::TYPE_BULK_STRING === substr($line, 0, 1)) {
                    --$i;
                } else if (self::TYPE_ARRAY === substr($line, 0, 1)) {
                    $i = $i - substr($line, 1) - 1;
                }

                array_push($tmp, $line);
            }

            $return = $this->prepare($tmp);
        } elseif ($length === 0) {
            $return = array();
        }

        return $return;
    }
}
