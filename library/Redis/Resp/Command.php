<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2014
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   php_redis
 * @since     2014.04.09.
 */
namespace Redis\Resp;

use Redis\Resp;
use Redis\Resp\Exception as RespException;

/**
 * Class Command
 */
class Command extends Resp {

    /**
     * @var array
     */
    protected $arguments = array();

    /**
     * Constructor
     *
     * @param string $commandName command name
     */
    public function __construct($commandName) {
        $this->setName($commandName);
    }

    /**
     * Returns command name.
     *
     * @return string
     */
    public function getName() {
        return $this->arguments[0];
    }

    /**
     * Sets up command name.
     *
     * @param string $commandName
     *
     * @return $this
     */
    public function setName($commandName) {
        $this->arguments[0] = strtoupper($commandName);

        return $this;
    }

    /**
     * Returns command arguments.
     *
     * @return array
     */
    public function getArguments() {
        return array_slice($this->arguments, 1);
    }

    /**
     * Add a command argument.
     *
     * @param array|int|string $values
     *
     * @return $this
     */
    public function addArgument($values) {
        if (!is_array($values)) {
            $values = array($values);
        }

        $this->arguments = array_merge($this->arguments, $values);

        return $this;
    }

    /**
     * Returns a RESP protocol command string.
     *
     * @return string
     * @throws RespException
     */
    public function toString() {
        $this->add(Resp::TYPE_ARRAY, $this->getCommandArray());

        return parent::toString();
    }

    /**
     * Return a instance.
     *
     * @param string $command command name
     *
     * @static
     * @return Command
     */
    public static function create($command) {
        return new self($command);
    }

    /**
     * Returns a special array it contains commands data.
     *
     * @see Resp::addArray
     * @return array
     */
    protected function getCommandArray() {
        $arguments = array();

        array_push($arguments,
                   array(
                           '_type'  => Resp::TYPE_BULK_STRING,
                           '_value' => $this->getName()
                   )
        );

        foreach ($this->getArguments() as $argument) {
            array_push($arguments,
                       array(
                               '_type'  => Resp::TYPE_BULK_STRING,
                               '_value' => $argument
                       )
            );
        }

        return $arguments;
    }
}
