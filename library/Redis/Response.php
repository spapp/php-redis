<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2014
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   php_redis
 * @since     2014.03.31.
 */
namespace Redis;

use Redis\Command\AbstractCommand;
use Redis\Resp;
use Redis\Resp\Exception as RespException;
use Redis\Response\Exception as ResponseException;
use Redis\Response\Filter;
use Redis\Response\Filter\AbstractFilter;


/**
 * Class \Redis\Response
 */
class Response {
    const GLUE_ERROR_MESSAGES = ' ';
    const NS                  = '\\Redis\\Response\\';

    const RESPONSE_PONG    = 'PONG';
    const RESPONSE_OK      = 'OK';
    const RESPONSE_SUCCESS = 1;
    const RESPONSE_FAIL    = 0;

    const RESPONSE_KEY_DOES_NOT_EXIST       = -2;
    const RESPONSE_HAS_NO_ASSOCIATED_EXPIRE = -1;

    const RESPONSE_TYPE_STRING = 'string';
    const RESPONSE_TYPE_LIST   = 'list';
    const RESPONSE_TYPE_SET    = 'set';
    const RESPONSE_TYPE_ZSET   = 'zset';
    const RESPONSE_TYPE_HASH   = 'hash';
    const RESPONSE_TYPE_NONE   = 'none';

    const FILTER_OK      = 'ok';
    const FILTER_SUCCESS = 'success';
    const FILTER_NUMBER  = 'number';

    /**
     * @var array
     * @static
     * @todo
     */
    protected static $filters = array(
            Command::COMMAND_PING         => 'pong',
            Command::COMMAND_QUIT         => self::FILTER_OK,
            Command::COMMAND_SELECT       => self::FILTER_OK,
            Command::COMMAND_AUTH         => self::FILTER_OK,
            //            Command::COMMAND_MIGRATE      => self::FILTER_OK,
            Command::COMMAND_RENAME       => self::FILTER_OK,
            Command::COMMAND_RESTORE      => self::FILTER_OK,
            Command::COMMAND_EXISTS       => self::FILTER_SUCCESS,
            Command::COMMAND_EXPIRE       => self::FILTER_SUCCESS,
            Command::COMMAND_EXPIREAT     => self::FILTER_SUCCESS,
            Command::COMMAND_MOVE         => self::FILTER_SUCCESS,
            Command::COMMAND_PERSIST      => self::FILTER_SUCCESS,
            Command::COMMAND_PEXPIRE      => self::FILTER_SUCCESS,
            Command::COMMAND_PEXPIREAT    => self::FILTER_SUCCESS,
            Command::COMMAND_RENAMENX     => self::FILTER_SUCCESS,
            Command::COMMAND_INFO         => 'info',
            Command::COMMAND_BGREWRITEAOF => self::FILTER_OK,
            Command::COMMAND_SET          => self::FILTER_OK,
            Command::COMMAND_FLUSHALL     => self::FILTER_OK,
            Command::COMMAND_FLUSHDB      => self::FILTER_OK,
            Command::COMMAND_SAVE         => self::FILTER_OK,
            Command::COMMAND_SLAVEOF      => self::FILTER_OK,
            Command::COMMAND_GETSET       => self::FILTER_NUMBER,
            Command::COMMAND_INCR         => self::FILTER_NUMBER,
            Command::COMMAND_INCRBY       => self::FILTER_NUMBER,
            Command::COMMAND_INCRBYFLOAT  => self::FILTER_NUMBER,
            Command::COMMAND_MGET         => 'mget',
            Command::COMMAND_MSET         => self::FILTER_OK,
            Command::COMMAND_MSETNX       => self::FILTER_SUCCESS,
            Command::COMMAND_PSETEX       => self::FILTER_OK,
            Command::COMMAND_SETEX        => self::FILTER_OK,
            Command::COMMAND_SETNX        => self::FILTER_SUCCESS,
            Command::COMMAND_HSET         => self::FILTER_SUCCESS,
            Command::COMMAND_HEXISTS      => self::FILTER_SUCCESS,
            Command::COMMAND_HGETALL      => 'hgetall',
            Command::COMMAND_HINCRBYFLOAT => self::FILTER_NUMBER,
            Command::COMMAND_HMGET        => 'hmget',
            Command::COMMAND_HMSET        => self::FILTER_OK,
            Command::COMMAND_HSETNX       => self::FILTER_SUCCESS,
            Command::COMMAND_LSET         => self::FILTER_OK,
            Command::COMMAND_LTRIM        => self::FILTER_OK,
            Command::COMMAND_SISMEMBER    => self::FILTER_SUCCESS,
            Command::COMMAND_SMOVE        => self::FILTER_SUCCESS,
            Command::COMMAND_ZINCRBY      => self::FILTER_NUMBER,
            Command::COMMAND_ZRANGE       => 'zrange',
            Command::COMMAND_ZREVRANGE    => 'zrevrange',
            Command::COMMAND_ZSCORE       => self::FILTER_NUMBER,
            Command::COMMAND_PFADD        => self::FILTER_SUCCESS,
            Command::COMMAND_PFMERGE      => self::FILTER_OK,
            Command::COMMAND_CLIENT       => 'client',
            Command::COMMAND_CONFIG       => 'config'

    );

    /**
     * @var mixed request depending on the value
     */
    protected $value = null;

    /**
     * @var Command
     */
    protected $command = null;

    /**
     * @var Resp
     */
    protected $resp = null;

    /**
     * @var AbstractFilter
     */
    protected $filter = null;

    /**
     * Constructor
     *
     * @param null|string|Resp $response
     *
     * @throws ResponseException
     */
    public function __construct($response = null) {
        if (null !== $response) {
            $this->setResp($response);
        }
    }

    /**
     * Set up a response filter.
     *
     * @param AbstractFilter $filter
     *
     * @return $this
     */
    public function setFilter(AbstractFilter $filter = null) {
        if (null === $filter and array_key_exists($this->getCommand()->getName(), self::$filters)) {
            $filter = Filter::factory(self::$filters[$this->getCommand()->getName()]);
        }

        if (null !== $filter) {
            $filter->setResponse($this);
        }

        $this->filter = $filter;

        return $this;
    }

    /**
     * Returns a response filter.
     *
     * @return AbstractFilter
     */
    public function getFilter() {
        return $this->filter;
    }

    /**
     * Returns _TRUE_ if filter is exists and it is implements filter interface.
     *
     * @return bool
     */
    public function hasFilter() {
        return ($this->filter and $this->filter instanceof AbstractFilter);
    }

    /**
     * Returns command.
     *
     * @return AbstractCommand
     */
    public function getCommand() {
        return $this->command;
    }

    /**
     * Sets up command.
     *
     * @param AbstractCommand $command
     *
     * @return $this
     */
    public function setCommand(AbstractCommand $command) {
        $this->command = $command;
        $this->setFilter();

        return $this;
    }

    /**
     * Returns REdis Serialization Protocol object.
     *
     * @return Resp
     */
    public function getResp() {
        return $this->resp;
    }

    /**
     * Sets up REdis Serialization Protocol object.
     *
     * @param string|Resp $resp
     *
     * @return $this
     * @throws ResponseException
     */
    public function setResp($resp) {
        if (is_string($resp)) {
            $resp = new Resp($resp);
        } elseif (!($resp instanceof Resp)) {
            throw new ResponseException('Not supported response type.');
        }

        $this->resp = $resp;

        return $this;
    }

    /**
     * Sets up response value.
     *
     * @return $this
     * @throws RespException
     */
    public function setValue() {
        if (null === $this->value) {
            $this->value = $this->getResp()->getValue();
        }

        return $this;
    }

    /**
     * Returns response value.
     *
     * If the response contains some errors then throw a exception.
     *
     * @return mixed
     * @throws RespException
     */
    public function getValue() {
        $this->setValue();

        if ($this->hasFilter()) {
            return $this->getFilter()->filter($this->value);
        }

        return $this->value;
    }

    /**
     * Returns raw redis response.
     *
     * @return string
     */
    public function getRawValue() {
        return (string)$this->getResp();
    }

    /**
     * Returns error messages.
     *
     * @return array
     * @throws \Redis\Resp\Exception
     */
    public function getErrors() {
        $this->setValue();

        return $this->getResp()->getErrors();
    }

    /**
     * Returns error messages as a string.
     *
     * @see Redis_Response::GLUE_ERROR_MESSAGES
     * @return string
     */
    public function getErrorsAsString() {
        return implode(self::GLUE_ERROR_MESSAGES, $this->getErrors());
    }

    /**
     * Returns an error object with error messages.
     *
     * @see Response::getErrorsAsString
     * @return \Redis\Response\Exception
     */
    public function getError() {
        $error = new ResponseException($this->getErrorsAsString());
        $error->setResponse($this);

        return $error;
    }

    /**
     * Returns _TRUE_ if error happens.
     *
     * @return bool
     */
    public function hasError() {
        return (bool)count($this->getErrors());
    }

    /**
     * Creates a response object from the response text.
     *
     * @param string $response
     *
     * @static
     * @return Response
     */
    public static function create($response = null) {
        return new self($response);
    }
}
