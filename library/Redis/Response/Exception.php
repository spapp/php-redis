<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2014
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   php_redis
 * @since     2014.03.31.
 */
namespace Redis\Response;

use Redis\Response;

/**
 * Class Exception
 */
class Exception extends \Exception {
    /**
     * @var Response
     */
    protected $response = null;

    /**
     * Returns response.
     *
     * @return Response
     */
    public function getResponse() {
        return $this->response;
    }

    /**
     * Sets up response.
     *
     * @param Response $response
     *
     * @return $this
     */
    public function setResponse(Response $response) {
        $this->response = $response;

        return $this;
    }
}
