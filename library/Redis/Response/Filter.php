<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2014
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   php_redis
 * @since     2014.04.15.
 */
namespace Redis\Response;

/**
 * Class ResponseFilter
 */
class Filter {
    const NS = 'Redis\\Response\\Filter\\';
    /**
     * Constructor
     */
    public final function __construct() {

    }

    /**
     * Returns a response filter.
     *
     * @param array|string $name    filter name
     * @param null|array   $options filter options
     *
     * @static
     * @return \Redis\Filter\FilterInterface
     */
    public static function factory($name, $options = null) {
        if (is_array($name)) {
            $options = $name['options'];
            $name    = $name['class'];
        }

        if (false === strpos($name, self::NS)) {
            $name = self::NS . ucfirst($name);
        }

        return new $name($options);
    }
}
