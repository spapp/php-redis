<?php
/**
 * @author     Sándor Papp <spapp@spappsite.hu>
 * @copyright  2014
 * @license    http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package    php_redis
 * @subpackage Redis\Response\Filter
 * @since      2014.05.21.
 */

namespace Redis\Response\Filter;

use Redis\Filter\FilterInterface;
use Redis\Response;

/**
 * Class AbstractFilter
 */
abstract class AbstractFilter implements FilterInterface {

    /**
     * @var Response
     */
    protected $response = null;

    /**
     * Returns response object.
     *
     * @return Response
     */
    public function getResponse() {
        return $this->response;
    }

    /**
     * Set up response object.
     *
     * @param Response $response
     *
     * @return $this
     */
    public function setResponse(Response $response) {
        $this->response = $response;

        return $this;
    }

} 