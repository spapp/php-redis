<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2014
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   php_redis
 * @since     2014.04.14.
 */
namespace Redis\Response\Filter;

/**
 * Class Boolean
 */
class Boolean extends AbstractFilter {

    /**
     * @var mixed boolean _TRUE_ value
     */
    protected $trueValue = true;

    /**
     * Constructor
     *
     * @param bool $trueValue
     */
    public function __construct($trueValue = true) {
        $this->setTrueValue($trueValue);
    }

    /**
     * Set up _TRUE_ value.
     *
     * @param mixed $trueValue
     *
     * @return $this
     */
    public function setTrueValue($trueValue) {
        $this->trueValue = $trueValue;

        return $this;
    }

    /**
     * Returns boolean _TRUE_ value
     *
     * @return mixed
     */
    public function getTrueValue() {
        return $this->trueValue;
    }

    /**
     * Returns _TRUE_ if $value is equal $trueValue.
     *
     * @param mixed $value
     *
     * @return bool
     */
    public function filter($value) {
        return ($this->getTrueValue() === $value);
    }
}
