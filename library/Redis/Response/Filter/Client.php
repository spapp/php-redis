<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2014
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   php_redis
 * @since     2014.06.12.
 */
namespace Redis\Response\Filter;

use Redis\Client as RedisClient;
use Redis\Resp;
use Redis\Response;

/**
 * Class Client
 */
class Client extends AbstractFilter {

    /**
     * Returns _TRUE_ if $value is equal $trueValue.
     *
     * @param mixed $value
     *
     * @return bool
     */
    public function filter($value) {
        $return   = array();
        $arguments = ($this->getResponse()->getCommand()->getArguments());

        switch (strtoupper(array_shift($arguments))) {
            case RedisClient::COMMAND_CLIENT_GETNAME:
                $return = $value;
                break;
            case RedisClient::COMMAND_CLIENT_LIST:
                $value = explode("\n", trim($value));

                foreach ($value as &$v) {
                    $client = array();
                    $v      = explode(' ', trim($v));

                    foreach ($v as &$vv) {
                        $vv = explode('=', trim($vv));

                        if (is_numeric($vv[1])) {
                            $client[$vv[0]] = $vv[1] * 1;
                        } else {
                            $client[$vv[0]] = $vv[1];
                        }
                    }

                    array_push($return, $client);
                }
                break;
            case RedisClient::COMMAND_CLIENT_PAUSE:
            case RedisClient::COMMAND_CLIENT_SETNAME:
            case RedisClient::COMMAND_CLIENT_KILL:
                $return = (Response::RESPONSE_OK === $value);
                break;
        }

        return $return;
    }
}
