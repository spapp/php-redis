<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2014
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   php_redis
 * @since     2014.06.13.
 */
namespace Redis\Response\Filter;

use Redis\Client as RedisClient;
use Redis\Resp;
use Redis\Response;

/**
 * Class Config
 */
class Config extends AbstractFilter {

    /**
     * Returns _TRUE_ if $value is equal $trueValue.
     *
     * @param mixed $value
     *
     * @return bool
     */
    public function filter($value) {
        $return   = array();
        $arguments = ($this->getResponse()->getCommand()->getArguments());

        switch (strtoupper(array_shift($arguments))) {
            case RedisClient::COMMAND_CONFIG_SET:
                $return = (Response::RESPONSE_OK === $value);
                break;
            case RedisClient::COMMAND_CONFIG_GET:
                while(count($value)>0){
                    $return[array_shift($value)] = array_shift($value);
                }
                break;
            case RedisClient::COMMAND_CONFIG_RESETSTAT:
            case RedisClient::COMMAND_CONFIG_REWRITE:
                $return = (Response::RESPONSE_OK === $value);
                break;
        }

        return $return;
    }
}
