<?php
/**
 * @author     Sándor Papp <spapp@spappsite.hu>
 * @copyright  2014
 * @license    http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package    php_redis
 * @subpackage Redis\Response\Filter
 * @since      2014.05.22.
 */

namespace Redis\Response\Filter;

/**
 * Class Hgetall
 */
class Hgetall extends AbstractFilter {
    /**
     * Returns response as key and value pairs.
     *
     * @param array $value
     *
     * @return array
     */
    public function filter($value) {
        $return = array();

        while (count($value) > 0) {
            $return[array_shift($value)] = array_shift($value);
        }

        return $return;
    }
}