<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2014
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   php_redis
 * @since     2014.04.15.
 */
namespace Redis\Response\Filter;

use Redis\Resp;

/**
 * Class Info
 */
class Info extends AbstractFilter {
    /**
     * Returns response as a array.
     *
     * @param string $value
     *
     * @return bool
     */
    public function filter($value) {
        $filteredValue = $this->prepareToArray($value, Resp::EOL, ':');

        if (1 === count($filteredValue)) {
            $filteredValue = array_shift($filteredValue);
        }

        return $filteredValue;
    }

    /**
     * Helper function for filter.
     *
     * Returns redis info string as a array.
     *
     * @param string $info
     * @param string $glueLine
     * @param string $glueKeyValue
     *
     * @return array|mixed
     */
    protected function prepareToArray($info, $glueLine, $glueKeyValue) {
        $return  = array();
        $info    = explode($glueLine, $info);
        $section = null;

        while (count($info) > 0) {
            $line = trim(array_shift($info));

            if ('#' === substr($line, 0, 1)) {
                $section          = strtolower(trim(substr($line, 1)));
                $return[$section] = array();
            } elseif ($line) {
                list($name, $value) = explode($glueKeyValue, $line, 2);

                if (is_numeric($value)) {
                    $value *= 1;
                } elseif (false !== strpos($value, '=')) {
                    $value = $this->prepareToArray($value, ',', '=');
                }

                if ($section) {
                    $return[$section][$name] = $value;
                } else {
                    $return[$name] = $value;
                }


            }
        }

        return $return;
    }
}
