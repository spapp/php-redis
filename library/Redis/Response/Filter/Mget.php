<?php
/**
 * @author     Sándor Papp <spapp@spappsite.hu>
 * @copyright  2014
 * @license    http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package    php_redis
 * @subpackage Redis\Response\Filter
 * @since      2014.05.21.
 */

namespace Redis\Response\Filter;

/**
 * Class Mget
 */
class Mget extends AbstractFilter {
    /**
     * Returns response as key and value pairs.
     *
     * @param array $value
     *
     * @return array
     */
    public function filter($value) {
        return array_combine($this->getResponse()->getCommand()->getArguments(), $value);
    }
} 