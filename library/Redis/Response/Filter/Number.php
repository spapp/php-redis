<?php
/**
 * @author     Sándor Papp <spapp@spappsite.hu>
 * @copyright  2014
 * @license    http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package    php_redis
 * @subpackage Redis\Response\Filter
 * @since      2014.05.21.
 */

namespace Redis\Response\Filter;

/**
 * Class Number
 */
class Number extends AbstractFilter {
    /**
     * Constructor
     */
    public function __construct() {

    }

    /**
     * Returns a number value.
     *
     * @param mixed $value
     *
     * @return number
     */
    public function filter($value) {
        return is_null($value) ? null : ($value * 1);
    }
}