<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2014
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   php_redis
 * @since     2014.04.15.
 */
namespace Redis\Response\Filter;

use Redis\Response;

/**
 * Class Ok
 */
class Ok extends Boolean {

    /**
     * Constructor
     */
    public function __construct() {
        parent::__construct(Response::RESPONSE_OK);
    }
}
