<?php
/**
 * @author     Sándor Papp <spapp@spappsite.hu>
 * @copyright  2014
 * @license    http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package    php_redis
 * @subpackage Redis\Response\Filter
 * @since      2014.05.23.
 */

namespace Redis\Response\Filter;

use Redis\Client;
use Redis\Command;

/**
 * Class Zrange
 */
class Zrange extends AbstractFilter {
    /**
     * Returns response as key and value pairs.
     *
     * @param array $value
     *
     * @return array
     */
    public function filter($value) {
        $arguments = $this->getResponse()->getCommand()->getArguments();
        $return    = array();

        if (4 === count($arguments) and Client::COMMAND_ZRANGE_WITHSCORES === $arguments[3]) {
            while (count($value) > 0) {
                $return[array_shift($value)] = array_shift($value);
            }
        } else {
            $return = $value;
        }


        return $return;
    }
} 