<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2014
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   php_redis
 * @since     2014.04.18.
 */
namespace Redis\Response;

/**
 * Class Object
 */
class Object implements \Iterator {
    /**
     * @var array
     */
    protected $value = array();

    /**
     * Constructor
     */
    public function __construct(array $value = null) {
        if ($value) {
            $this->setValue($value);
        }
    }

    /**
     * Returns base value as an array.
     *
     * @return array
     */
    public function toArray() {
        return $this->value;
    }

    /**
     * Sets the base value.
     *
     * @param array $value
     *
     * @return $this
     */
    public function setValue(array $value) {
        $this->value = $value;

        return $this;
    }

    public function __call($name, $arguments) {

    }

    /**
     * Returns the current element
     *
     * If the current element is an array, then returns a Redis_Response_Object.
     * It does not move the pointer in any way.
     * If the internal pointer points beyond the end of the elements list or the array is empty, then returns _FALSE_.
     *
     * @return mixed|Object
     */
    public function current() {
        $current = current($this->value);

        if (is_array($current)) {
            $current = new self($current);
        }

        return $current;
    }

    /**
     * Returns the key of the current element
     *
     * It does not move the pointer in any way.
     * If the internal pointer points beyond the end of the elements list or the array is empty, then returns _NULL_.
     *
     * @return mixed
     */
    public function key() {
        return key($this->value);
    }

    /**
     * Move forward to next element
     *
     * Returns the array value in the next place that's pointed to by the internal array pointer,
     * or _FALSE_ if there are no more elements.
     *
     * @return mixed|void
     */
    public function next() {
        return next($this->value);
    }

    /**
     * Rewind the Iterator to the first element
     *
     * Returns the value of the first array element, or _FALSE_ if the array is empty.
     *
     * @return mixed|bool
     */
    public function rewind() {
        return reset($this->value);
    }

    /**
     * Checks if current position is valid
     *
     * @return bool
     */
    public function valid() {
        return (key($this->value) !== null);
    }
} 