<?php

/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2014
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   php_redis
 * @since     2014.03.31.
 */
namespace Redis\Traits;

trait OptionTrait {
    /**
     * @var array options container
     */
    protected $options = array();

    /**
     * Sets up more options.
     *
     * @param array $options
     *
     * @return $this
     */
    public function  setOptions(array $options) {
        foreach ($options as $name => $value) {
            $this->setOption($name, $value);
        }

        return $this;
    }

    /**
     * Set up an option value.
     *
     * @param string $name
     * @param mixed  $value
     *
     * @return $this
     */
    public function  setOption($name, $value) {
        $this->options[$name] = $value;

        return $this;
    }

    /**
     * Returns an option value if it is exists otherwise returns the $default value.
     *
     * @param string $name
     * @param mixed  $default
     *
     * @return null
     */
    public function  getOption($name, $default = null) {
        if (true === $this->hasOption($name)) {
            $default = $this->options[$name];
        }

        return $default;
    }

    /**
     * Returns _TRUE_ if the option is setted.
     *
     * @param string $name
     *
     * @return bool
     */
    public function  hasOption($name) {
        return isset($this->options[$name]);
    }
}
