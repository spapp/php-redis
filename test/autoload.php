<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2014
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   php_redis
 * @since     2014.04.07.
 */

ini_set('display_errors', true);
ini_set('display_startup_errors', true);
error_reporting(E_ALL | E_STRICT | E_DEPRECATED);

define('APPLICATION_PATH', dirname(__DIR__));

require_once(dirname(APPLICATION_PATH) . '/php-autoloader/library/Autoloader.php');

Autoloader::getInstance()
          ->addIncludePath(APPLICATION_PATH . '/library')
          ->addIncludePath(APPLICATION_PATH . '/test/library')
          ->register();