<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2014
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   php_redis
 * @since     2014.05.14.
 */
namespace Redis;

    /*

        const COMMAND_QUIT   = 'QUIT';
        const COMMAND_SELECT = 'SELECT';
        const COMMAND_AUTH   = 'AUTH';
        */

/**
 * Class ConnectionTest
 */
class ConnectionTest extends DbConnection {
    public function testCommandPing() {
        $response = $this->client->ping();

        $rawAnswer = Resp::TYPE_STRING . Response::RESPONSE_PONG . Resp::EOL . Resp::EOL;

        $this->assertTrue(($rawAnswer === $response->getRawValue()));
        $this->assertTrue($response->getValue());
    }

    public function testCommandEcho() {
        $msg       = 'Echo test.';
        $rawAnswer = Resp::TYPE_BULK_STRING . strlen($msg) . Resp::EOL . $msg . Resp::EOL . Resp::EOL;

        $response = $this->client->echo($msg);


        $this->assertTrue(($rawAnswer === $response->getRawValue()));
        $this->assertTrue(($msg === $response->getValue()));
    }

    public function testCommandSelect() {
        $response = $this->client->select(1);

        $this->assertTrue(($this->rawOKAnswer === $response->getRawValue()));
        $this->assertTrue((true === $response->getValue()));
    }

    public function testCommandQuit() {
        $response = $this->client->quit();

        $this->assertTrue(($this->rawOKAnswer === $response->getRawValue()));
        $this->assertTrue((true === $response->getValue()));
    }
} 