<?php
/**
 * @author     Sándor Papp <spapp@spappsite.hu>
 * @copyright  2014
 * @license    http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package    php_redis
 * @subpackage Redis
 * @since      2014.05.15.
 */

namespace Redis;

/**
 * Class KeysTest
 */
class KeysTest extends DbConnection {

    protected $testData = array(
            'AA-sdskjjkfg'     => 'opr5986986',
            'BB-er345'         => 'jhlkéé',
            'AA-456fggjkéáőűü' => '43539487rtet',
            'CC-jképáőüöó5e'   => 'sdf56486rt8qggIUJI'
    );

    protected $testDataNotExists = array();

    public function setUp() {
        parent::setUp();

        if ($this->client->dbsize()->getValue() > 0) {
            call_user_func_array(array(
                                         $this->client,
                                         'del'
                                 ),
                                 (array)$this->client->keys('*')->getValue()
            );
        }

        foreach ($this->testData as $key => $value) {
            $this->client->set($key, $value);

            array_push($this->testDataNotExists, $key . '-' . microtime(true));
        }
    }

    public function testCommandKeys() {
        $allKeys = $this->client->keys('*')->getValue();
        $AAKeys  = $this->client->keys('AA-*')->getValue();
        $BBKeys  = $this->client->keys('BB-*')->getValue();
        $CCKeys  = $this->client->keys('CC-*')->getValue();

        foreach ($allKeys as $key) {
            $this->assertTrue(array_key_exists($key, $this->testData));
        }

        foreach ($AAKeys as $key) {
            $this->assertTrue(array_key_exists($key, $this->testData));
            $this->assertTrue(('AA-' === substr($key, 0, 3)));
        }

        foreach ($BBKeys as $key) {
            $this->assertTrue(array_key_exists($key, $this->testData));
            $this->assertTrue(('BB-' === substr($key, 0, 3)));
        }

        foreach ($CCKeys as $key) {
            $this->assertTrue(array_key_exists($key, $this->testData));
            $this->assertTrue(('CC-' === substr($key, 0, 3)));
        }
    }

    public function testCommandExists() {
        $rawAnswerExists    = Resp::TYPE_INTEGER . '1' . Resp::EOL . Resp::EOL;
        $rawAnswerNotExists = Resp::TYPE_INTEGER . '0' . Resp::EOL . Resp::EOL;

        foreach ($this->testData as $key => $value) {
            $response = $this->client->exists($key);

            $this->assertTrue($rawAnswerExists === $response->getRawValue());
            $this->assertTrue(true === $response->getValue());
        }

        foreach ($this->testDataNotExists as $key) {
            $response = $this->client->exists($key);

            $this->assertTrue($rawAnswerNotExists === $response->getRawValue());
            $this->assertTrue(false === $response->getValue());
        }

    }

    public function testCommandDel() {
        $rawAnswer = Resp::TYPE_INTEGER . '1' . Resp::EOL . Resp::EOL;
        $key       = $this->client->RANDOMKEY()->getValue();
        $response  = $this->client->del($key);

        $this->assertTrue($rawAnswer === $response->getRawValue());
        $this->assertTrue(1 === $response->getValue());
        $this->assertTrue(false === $this->client->exists($key)->getValue());
    }

    public function testCommandDumpRestore() {
        $key          = $this->client->RANDOMKEY()->getValue();
        $restoreKey   = $key . '-0';
        $value        = $this->client->dump($key)->getValue();
        $response     = $this->client->restore($restoreKey, 0, $value);
        $restoreValue = $this->client->dump($restoreKey)->getValue();


        $this->assertTrue(true === $response->getValue());
        $this->assertTrue($value === $restoreValue);
    }
}

/**
 * const COMMAND_EXPIRE    = 'EXPIRE';
 * const COMMAND_EXPIREAT  = 'EXPIREAT';
 * const COMMAND_MOVE      = 'MOVE';
 * const COMMAND_PERSIST   = 'PERSIST';
 * const COMMAND_PEXPIRE   = 'PEXPIRE';
 * const COMMAND_PEXPIREAT = 'PEXPIREAT';
 * const COMMAND_PTTL      = 'PTTL';
 * const COMMAND_RANDOMKEY = 'RANDOMKEY';
 * const COMMAND_RENAME    = 'RENAME';
 * const COMMAND_RENAMENX  = 'RENAMENX';
 * const COMMAND_TTL       = 'TTL';
 * const COMMAND_TYPE      = 'TYPE';
 */