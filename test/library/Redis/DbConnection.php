<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2014
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   php_redis
 * @subpackage Redis
 * @since     2014.05.15.
 */

namespace Redis;

use PHPUnit_Framework_TestCase as TestCase;

/**
 * Class DbConnection
 */
class DbConnection extends TestCase {

    /**
     * @var Client
     */
    protected $client  = null;

    protected $rawOKAnswer = null;

    protected $options = array(
            Connection::OPTION_TIMEOUT            => TEST_DB_TIMEOUT,
            Connection::OPTION_CONNECTION_TIMEOUT => TEST_DB_CONNECTION_TIMEOUT,
            Connection::OPTION_PROTOCOL           => TEST_DB_PROTOCOL,
            Connection::OPTION_HOST               => TEST_DB_HOST,
            Connection::OPTION_PORT               => TEST_DB_PORT,
            Connection::OPTION_PATH               => '',
            Connection::OPTION_ASYNC              => false,
            Connection::OPTION_PERSIST            => false
    );

    public function setUp() {
        $this->client = Client::create($this->options);
        $this->client->select(TEST_DB_INDEX);
        $this->client->setResponseFetchMode(Client::FETCH_MODE_OBJECT);

        $this->rawOKAnswer = Resp::TYPE_STRING . Response::RESPONSE_OK . Resp::EOL . Resp::EOL;
    }

    public function tearDown() {
        unset($this->client);
    }
} 