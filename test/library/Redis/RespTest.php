<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2014
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   php_redis
 * @since     2014.04.07.
 */
namespace Redis;

use PHPUnit_Framework_TestCase as TestCase;
use Redis\Command;
use Redis\Resp;
use Redis\Response;

/**
 * Class RespTest
 */
class RespTest extends TestCase {
    public function testRequestPing() {
        $resp = new Resp();

        $cmd = array(
                array(
                        '_type'  => Resp::TYPE_BULK_STRING,
                        '_value' => Command::COMMAND_PING
                )
        );

        $resp->add(Resp::TYPE_ARRAY, $cmd);
        $this->assertEquals("*1\r\n$4\r\nPING\r\n", $resp->toString());
    }

    public function testResponseArray() {
        $resp = new Resp("*3\r\n$3\r\nfoo\r\n$-1\r\n$3\r\nbar\r\n");
        $this->assertEquals('["foo",null,"bar"]', json_encode($resp->getValue()));

        $resp->setValue("*2\r\n*3\r\n:1\r\n:2\r\n:3\r\n*2\r\n+Foo\r\n+Bar\r\n");
        $this->assertEquals('[[1,2,3],["Foo","Bar"]]', json_encode($resp->getValue()));
    }

    public function testResponseBulkString() {
        $resp = new Resp("$6\r\nfoobar\r\n");
        $this->assertEquals('foobar', $resp->getValue());

        $resp->setValue("$0\r\n\r\n");

        $this->assertEquals('', $resp->getValue());
    }

    public function testResponseString() {
        $resp = new Resp("+OK\r\n");
        $this->assertEquals(Response::RESPONSE_OK, $resp->getValue());
    }

    public function testResponseNull() {
        $resp = new Resp("$-1\r\n");
        $this->assertEquals(true, is_null($resp->getValue()));
    }
} 